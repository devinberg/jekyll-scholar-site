# Jekyll Scholar Site
This repository contains the source files for a template Jekyll Scholar Site. You can see an [example site here](https://devinberg.gitlab.io/jekyll-scholar-site/).



## Contributing
Feel free to submit a PR if you think you have a relevant change.

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />
This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
See the LICENSE.txt file or follow the link for details.
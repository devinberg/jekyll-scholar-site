---
title: Citation example
tags: [informational, meta]
category: meta
post_author: J. Jones
author_url: https://www.example.com
layout: post
comments: true
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis 'aute irure dolor in reprehenderit in voluptate'
velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
cupidatat non proident, 'sunt in culpa qui officia deserunt mollit anim id est
laborum' {% cite ruby %}.


## References
{% bibliography --cited %}
---
title: Math example
tags: [informational, meta, math, equations]
category: meta
post_author: D. Davis
author_url: https://www.example.com
layout: post
comments: true
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis 'aute irure dolor in reprehenderit in voluptate'
velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
cupidatat non proident, 'sunt in culpa qui officia deserunt mollit anim id est
laborum'.


Equations can be written and rendered using [MathJax](https://docs.mathjax.org/en/latest/#). This allows the insertion of Latex formatted expressions like Einstein's classic {% cite einstein1905erzeugung %}.

$$ E=mc^2 $$

These equations can include basically all of the normal Latex math operators. See an example using the quadratic formula below {% cite abramowitz1965handbook %}.

$$ x=\frac{-b\pm\sqrt{b^2-4ac}}{2a} $$


## References
{% bibliography --cited %}
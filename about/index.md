---
layout: default
---

# About Jekyll Scholar Site	

This site is meant to serve as a template for creating a blog in Jekyll and using scholarly markdown.

Features included:  
* Static site generation using [Jekyll][]
* Source code hosting on [GitLab][] at my [GitLab repository](https://gitlab.com/devinberg/jekyll-scholar-site)
* The site is served from [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html)
* CSS from [Bootstrap](http://getbootstrap.com/)
* Theme from [Bootswatch](http://bootswatch.com/)
* Markdown processing using [kramdown](https://kramdown.gettalong.org/)
* Indexing and search using [Lunr.js](https://lunrjs.com/)
* Annotation with [Hypothes.is](https://web.hypothes.is)
* Commenting powered by [Disqus](https://disqus.com/)
* Citation management using [Jekyll-Scholar](https://github.com/inukshuk/jekyll-scholar)

[Jekyll]: http://jekyllrb.com
[GitLab]: https://gitlab.com/